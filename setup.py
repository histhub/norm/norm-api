#!./venv/bin/python

import codecs
import os

from setuptools import setup, find_packages

package_name = "norm_api"
min_python_version = (3, 4)

base = os.path.abspath(os.path.dirname(__file__))

try:
    from pip._internal.req import parse_requirements
except ImportError:
    try:
        from pip.req import parse_requirements
    except ImportError:
        raise RuntimeError("Could not find pip requirements parser")


def read_file(path):
    """Read file and return contents"""
    try:
        with codecs.open(path, "r", encoding="utf-8") as f:
            return f.read()
    except IOError:
        return ""


def get_reqs(target):
    """Parse requirements.txt files and return array"""
    reqs = parse_requirements(
        os.path.join(base, "requirements.d", ("%s.txt" % target)),
        session="hack")
    return [str(r.req) for r in reqs]


def get_meta(name):
    """Get metadata from project's __init__.py"""
    return getattr(__import__(package_name), name)


setup(
    name=package_name,
    version=get_meta("__version__"),

    description=get_meta("__description__"),
    long_description=read_file(os.path.join(base, "README.rst")),

    classifiers=[
        "Development Status :: 4 - Beta",
        "Programming Language :: Python :: 3 :: Only",
        "Environment :: No Input/Output (Daemon)",
        "Framework :: Bottle",
        "Intended Audience :: Information Technology",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],

    install_requires=get_reqs("app"),
    extras_require={
        "dev": get_reqs("development")
        },

    packages=find_packages(),
    python_requires=(">=%s" % ".".join(map(str, min_python_version))))
