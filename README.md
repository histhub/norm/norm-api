# Norm-API

This repository contains the histHub Norm DB API code.

## Set up

The easiest way to set up an instance of the Norm-API is to:
```console
$ make serve
```

It will automatically install the required dependencies into a local virtualenv
(`./venv`) and execute the built-in development web server.

It will complain about a missing DB URL, though. The next step is to tell the
Norm-API which database it is supposed to connect to.

A file named `settings.conf` must be created in the current working directory
(usually the repository root).
The provided [settings.conf.example](./settings.conf.example) can be used as a
template.
