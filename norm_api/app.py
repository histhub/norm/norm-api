import bottle
import logging
import os

from eavlib.db.url import make_url

from norm_api.plugins.normdb import NormDBPlugin

bottle.PROJECT_DIR = os.path.realpath(os.path.dirname(__file__))
bottle.TEMPLATE_PATH.insert(
    0, os.path.join(bottle.PROJECT_DIR, "views"))
bottle.TEMPLATE_PATH.insert(
    1, os.path.join(bottle.PROJECT_DIR, "docs", "views"))

# Initialise Bottle app
app = bottle.default_app()

# Load settings
settings_file = os.environ.get(
    "NORM_API_SETTINGS", os.getcwd() + "/settings.conf")
if not os.path.exists(settings_file):
    raise RuntimeError("Cannot find settings.conf at: %s" % settings_file)
app.config.load_config(settings_file)

# Allow configuration of logging through settings file
logging.basicConfig(**{
    k[8:]: v for k, v in app.config.items() if k.startswith("logging.")})

# Install norm DB plugin
normdb_plugin = NormDBPlugin(
    make_url(app.config.get("eavlib.url")),
    app.config.get("eavlib.model_name"),
    make_url(app.config.get("sidecar_db.url")))
app.install(normdb_plugin)

# Register API endpoints
from .routes import *  # noqa
