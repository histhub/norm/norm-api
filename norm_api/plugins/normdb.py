import bottle

from eavlib.connection import EAVConnection
from eavlib.graph import Graph
from normlib.connection import NormDBConnection
from normlib.db.sidecar_db import SidecarDBConnection


class NormDBPlugin:  # bottle.Plugin
    name = "normdb"
    api = 2

    def __init__(self, graph_url, model, sidecar_url):
        self.graph_url = graph_url
        self.model = model
        self.sidecar_url = sidecar_url

        self.normdb = None

    def setup(self, app):
        pass

    def _connect(self):
        try:
            self.normdb = NormDBConnection(
                Graph(
                    EAVConnection.fromstr(self.graph_url),
                    self.model),
                SidecarDBConnection(self.sidecar_url))
        except Exception:
            raise bottle.HTTPError(503, "Database Connection Failure")

    def __call__(self, callback):
        def wrapper(*args, **kwargs):
            if self.normdb is None \
                   or self.normdb.graph.connection.closed \
                   or self.normdb.sidecar_db.connection.closed:
                self._connect()

            for attempt in range(1, 4):
                try:
                    return callback(self.normdb, *args, **kwargs)
                except Exception as e:
                    if self.normdb.graph.connection.closed \
                       or self.normdb.sidecar_db.connection.closed \
                       or (e.__class__.__module__ == "psycopg2"
                           and e.__class__.__name__ in ("OperationalError", "InterfaceError", "InternalError")):
                        # reconnect and retry
                        print("reconnect to DB")
                        self._connect()
                        continue  # retry
                    # Rollback transaction on exception
                    self.normdb.graph.connection.rollback()
                    raise

        return wrapper

    def close(self):
        if self.normdb is not None:
            db = self.normdb
            self.normdb = None
            db.close()
            del db
