# -*- mode: yaml; indent-tabs-mode: nil; tab-width: 2 -*-
---
openapi: "3.0.2"

info:
  title: "histHub Norm API"
  description: |
    {{__description__}}
  license:
    name: "CC BY-SA 4.0"
    url: "https://creativecommons.org/licenses/by-nc-sa/4.0/"
  version: "{{__version__}}"

servers:
  - url: "{{base_url}}"

paths:
  /persons:
    parameters:
      - name: limit
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: offset
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: external_id
        in: query
        required: false
        allowEmptyValue: false
        schema:
          pattern: "[A-Za-z0-9_]+:[A-Za-z0-9]+"
      - name: label
        in: query
        required: false
        allowEmptyValue: false
    get:
      description: A paginated list of abstracts of all persons.
      responses:
        "200":
          description: A list of persons.
          content:
            application/json: {}
  /persons/{hhb_id}:
    parameters:
      - name: hhb_id
        in: path
        required: true
        schema:
          type: integer
          format: int64
          minimum: 10000000
    get:
      description: Returns detailed information about the person with the given histHub ID.
      responses:
        "200":
          description: The detailed information.
          content:
            application/json: {}
            text/html: {}
        "404":
          description: No such person exists.
        "406":
          description: The object with this ID is of a different type.
  /places:
    parameters:
      - name: limit
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: offset
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: external_id
        in: query
        required: false
        allowEmptyValue: false
        schema:
          pattern: "[A-Za-z0-9_]+:[A-Za-z0-9]+"
      - name: label
        in: query
        required: false
        allowEmptyValue: false
    get:
      description: A paginated list of abstracts of all places.
      responses:
        "200":
          description: A list of places.
          content:
            application/json: {}

  /places/{hhb_id}:
    parameters:
      - name: hhb_id
        in: path
        required: true
        schema:
          type: integer
          format: int64
          minimum: 10000000
    get:
      description: Returns detailed information about the place with the given histHub ID.
      responses:
        "200":
          description: The detailed information.
          content:
            application/json: {}
            text/html: {}
        "404":
          description: No such place exists.
        "406":
          description: The object with this ID is of a different type.
  /organizations:
    parameters:
      - name: limit
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: offset
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: external_id
        in: query
        required: false
        allowEmptyValue: false
        schema:
          pattern: "[A-Za-z0-9_]+:[A-Za-z0-9]+"
      - name: label
        in: query
        required: false
        allowEmptyValue: false
    get:
      description: A paginated list of abstracts of all organisations.
      responses:
        "200":
          description: A list of organisations.
          content:
            application/json: {}
  /organizations/{hhb_id}:
    parameters:
      - name: hhb_id
        in: path
        required: true
        schema:
          type: integer
          format: int64
          minimum: 10000000
    get:
      description: Returns detailed information about the organisation with the given histHub ID.
      responses:
        "200":
          description: The detailed information.
          content:
            application/json: {}
            text/html: {}
        "404":
          description: No such organisation exists.
        "406":
          description: The object with this ID is of a different type.
  /families:
    parameters:
      - name: limit
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: offset
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: external_id
        in: query
        required: false
        allowEmptyValue: false
        schema:
          pattern: "[A-Za-z0-9_]+:[A-Za-z0-9]+"
      - name: label
        in: query
        required: false
        allowEmptyValue: false
    get:
      description: A paginated list of abstracts of all families.
      responses:
        "200":
          description: A list of families.
          content:
            application/json: {}
  /families/{hhb_id}:
    parameters:
      - name: hhb_id
        in: path
        required: true
        schema:
          type: integer
          format: int64
    get:
      description: Returns detailed information about the family with the given histHub ID.
      responses:
        "200":
          description: The detailed information.
          content:
            application/json: {}
            text/html: {}
        "404":
          description: No such organisation exists.
        "406":
          description: The object with this ID is of a different type.
  /concepts:
    parameters:
      - name: limit
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: offset
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: external_id
        in: query
        required: false
        allowEmptyValue: false
        schema:
          pattern: "[A-Za-z0-9_]+:[A-Za-z0-9]+"
      - name: label
        in: query
        required: false
        allowEmptyValue: false
    get:
      description: A paginated list of abstracts of all concepts.
      responses:
        "200":
          description: A list of concepts.
          content:
            application/json: {}
  /concepts/{hhb_id}:
    parameters:
      - name: hhb_id
        in: path
        required: true
        schema:
          type: integer
          format: int64
          minimum: 10000000
    get:
      description: Returns detailed information about the concept with the given histHub ID.
      responses:
        "200":
          description: The detailed information.
          content:
            application/json: {}
            text/html: {}
        "404":
          description: No such concept exists.
        "406":
          description: The object with this ID is of a different type.
  /lemmas:
    parameters:
      - name: limit
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: offset
        in: query
        required: false
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
      - name: external_id
        in: query
        required: false
        allowEmptyValue: false
        schema:
          pattern: "[A-Za-z0-9_]+:[A-Za-z0-9]+"
      - name: label
        in: query
        required: false
        allowEmptyValue: false
    get:
      description: A paginated list of abstracts of all lemmas.
      responses:
        "200":
          description: A list of lemmas.
          content:
            application/json: {}
  /lemmas/{hhb_id}:
    parameters:
      - name: hhb_id
        in: path
        required: true
        schema:
          type: integer
          format: int64
          minimum: 10000000
    get:
      description: |
        Returns detailed information about the lemma with the given histHub ID.
      responses:
        "200":
          description: The detailed information.
          content:
            application/json: {}
            text/html: {}
        "404":
          description: No such lemma exists.
        "406":
          description: The object with this ID is of a different type.
  /nodes/{node_id}:
    parameters:
      - name: node_id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: hidden
        in: query
        required: false
        schema:
          type: boolean
      - name: max_depth
        in: query
        required: false
        schema:
          type: integer
    get:
      responses:
        "200":
          description: A generic view of a subgraph starting at `node_id`.
          content:
            application/json: {}
            application/xml: {}
        "404":
          description: No node with such ID present.
  /go:
    parameters:
      - name: entity
        in: query
        description: The entity type.
        required: false
        allowEmptyValue: false
        schema:
          type: string
      - name: hhb_id
        in: query
        description: The histHub ID.
        required: true
        allowEmptyValue: false
        schema:
          type: integer
          format: int64
          minimum: 10000000
    get:
      description: |
        Redirect to the "detail" endpoint containing information on
        the requested object.
        This endpoint is useful as the "action" of HTML forms to jump
        to a specific entity.
      responses:
        "303":
          description: Redirect to "detail" endpoint.
        "400":
          description: Invalid parameters.
        "404":
          description: No entity with such histHub ID present.
  /search:
    parameters:
      - name: entity
        in: query
        description: The entity type to search for.
        required: true
        allowEmptyValue: false
        schema:
          type: string
      - name: external_institution
        in: query
        description: The institution that handed out the `external_id`.
        required: false
        allowEmptyValue: true
        schema:
          type: string
      - name: external_id
        in: query
        description: The external ID to filter by.
        required: false
        allowEmptyValue: true
        schema:
          type: string
      - name: label
        in: query
        description: Filter entities whose label contains `label`.
        required: false
        allowEmptyValue: true
        schema:
          type: string
    get:
      description: |
        Search entities matching all of the given filter parameters.
      responses:
        "303":
          description: Redirect to results endpoint.
        "400":
          description: Invalid parameters.
