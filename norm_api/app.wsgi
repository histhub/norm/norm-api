# -*- mode: python; coding: utf-8 -*-

# Change working directory so relative paths (and template lookup) work again
# NOTE: This has been commented out, because we should let Apache to that
# import os
# os.chdir(os.path.dirname(__file__))

# Import WSGI application
# NOTE: Do NOT use bottle.run() with mod_wsgi
from norm_api.app import app as application  # noqa: F401
