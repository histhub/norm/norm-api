import bottle
import urllib.parse

VALID_ENTITIES = ("concept", "family", "lemma", "person", "place",
                  "organization")


def entity_plural(entity):
    return entity[:-1]+"ies" if entity.endswith("y") else entity+"s"


@bottle.get("/go")
def get_go(normdb):
    entity = bottle.request.query.pop("entity", None)
    if entity and entity.lower() not in VALID_ENTITIES:
        raise bottle.HTTPError(
            400, "invalid entity. Valid entities are: %s" % (
                tuple(sorted(VALID_ENTITIES)),))

    try:
        hhb_id = int(bottle.request.query.pop("hhb_id", 0))
    except ValueError:
        raise bottle.HTTPError(400, "invalid hhb_id")
    if hhb_id <= 0:
        raise bottle.HTTPError(
            400, "the hhb_id query parameter is mandatory and must be positive")

    if not entity:
        # Determine correct entity automatically
        try:
            entity = next(normdb.graph.traversal().v()
                          .has("hhb_id", hhb_id).label())
        except StopIteration:
            raise bottle.HTTPError(404, "no such entity")

    return bottle.redirect("/%s/%u" % (
        entity_plural(entity).lower(), hhb_id))


@bottle.get("/search")
def get_search(normdb):
    query = bottle.request.query.decode()
    entity = query.pop("entity", None)

    if not entity:
        raise bottle.HTTPError(400, "the entity query parameter is mandatory")
    elif entity.lower() not in VALID_ENTITIES:
        raise bottle.HTTPError(
            400, "invalid entity. Valid entities are: %s" % (
                tuple(sorted(VALID_ENTITIES)),))

    for k in [k for k in query if not query[k]]:
        # Remove empty query items
        del query[k]

    # HACK: Convert the external ID filter to a format accepted by the
    #       collection routes
    if "external_institution" in query or "external_id" in query:
        ext_inst = query.pop("external_institution", None)
        ext_id = query.pop("external_id", None)
        if ext_inst and ext_id:
            query["external_id"] = "%s:%s" % (ext_inst, ext_id)
        elif ext_id:
            query["external_id"] = ext_id

    return bottle.redirect("/%s?%s" % (
        entity_plural(entity), urllib.parse.urlencode(query, doseq=True)))
