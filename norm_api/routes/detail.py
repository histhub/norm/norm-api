import bottle

from eavlib.gremlin.traversal import __

from norm_api.util.batch import (
    apply_template, TplCast, TplDictList, TplList, TplSwitch)
from norm_api.util.graph import lookup_node_id
from norm_api.util.misc import (maybeint, strtobool)
from norm_api.util.route import autoview
from norm_api.util.tpl_snippets import (
    AttributionsSnippet, DateSnippet, FamilyNameSnippet, HHbIDSnippet,
    IDandLabelsSnippet, LabelsSnippet, LicenseSnippet,
    PersonNameUsageDictSnippet, ThesaurusConceptSnippet, ValueMapList)


def render_details(normdb, entity_name, hhb_id, template):
    if not hhb_id or hhb_id < 0:
        raise bottle.HTTPError(400, "hhb_id must be positive")

    graph = normdb.graph
    node_id = lookup_node_id(graph, entity_name, hhb_id)
    if node_id is None:
        raise bottle.HTTPError(404, "No such Entity")
    return apply_template(template, graph.traversal().v(node_id), omit_empty=True)[0]


@bottle.get("/persons/<hhb_id:int>")
@bottle.get("/persons/<hhb_id:int>.json")
@bottle.get("/persons/<hhb_id:int>.html")
@autoview("details_person")
def get_detail_person(normdb, hhb_id):
    return render_details(normdb, "Person", hhb_id, {
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "node_id": __.id(),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),

        "labels": LabelsSnippet(__),  # 1-n

        "sexes": TplDictList(__.out("sexes"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type"))  # 1-1
            }),

        "names": TplDictList(__.out("used_names"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "name": PersonNameUsageDictSnippet(__),  # 1-1
            "type": ThesaurusConceptSnippet(__.out("type"))  # 1-1
            }),

        "events": TplDictList(__.out("events"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "what": ThesaurusConceptSnippet(__.out("what")),  # 1-1
            "when": DateSnippet(__.out("when")),  # 1-1
            "where": TplDictList(
                __.out("where"), IDandLabelsSnippet(__.identity())),  # 0-n
            }),

        "relations": TplDictList(__.out("relations"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "person": IDandLabelsSnippet(__.out("other_person")),  # 1-1
            }),

        "relations_to_place": TplDictList(__.out("relations_to_place"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "place": IDandLabelsSnippet(__.out("other_place")),  # 1-1
            }),

        "occupations": TplDictList(__.out("occupations"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "organization": IDandLabelsSnippet(__.out("employer")),  # 0-1
            "place": IDandLabelsSnippet(__.out("place")),  # 0-1
            }),

        "titles": TplDictList(__.out("used_names").where(__.out("title")), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("title"))  # 1-1
            }),
        })


@bottle.get("/places/<hhb_id:int>")
@bottle.get("/places/<hhb_id:int>.json")
@bottle.get("/places/<hhb_id:int>.html")
@autoview("details_place")
def get_detail_place(normdb, hhb_id):
    return render_details(normdb, "Place", hhb_id, {
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "node_id": __.id(),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),

        "labels": LabelsSnippet(__),  # 1-n

        "types": TplDictList(__.out("used_types"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "function": ThesaurusConceptSnippet(__.out("function"))  # 0-1
            }),

        "names": TplDictList(__.out("used_names"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "name": __.out("name").values("text"),  # 1-1
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "lang": __.out("name").values("lang")  # 1-1
            }),

        "localisations": TplDictList(__.out("used_locations"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "localisation": TplSwitch(__.out("location").label(), {  # 1-1
                "Geometry": TplCast(dict, __.out("location").value_map()),
                "FreeTextLocation": TplCast(
                    lambda labels: {
                        label["lang"]: label["text"] for label in labels
                        } if labels is not None else {},
                    ValueMapList(  # 1-n
                        __.out("location").out("definitions").value_map())),
                "SpatialRelation": {
                    "type": ThesaurusConceptSnippet(  # 1-1
                        __.out("location").out("type")),
                    "place": IDandLabelsSnippet(__.out("location").out("other_place")),  # 1-1
                    },
                }),
            }),

        "events": TplDictList(__.out("events"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "what": ThesaurusConceptSnippet(__.out("what")),  # 1-1
            "when": DateSnippet(__.out("when")),  # 1-1
            "where": TplDictList(
                __.out("where"), IDandLabelsSnippet(__.identity())),  # 0-n
            }),
        })


# XXX: orgs is legacy
@bottle.get("/organizations/<hhb_id:int>")
@bottle.get("/organizations/<hhb_id:int>.json")
@bottle.get("/organizations/<hhb_id:int>.html")
@bottle.get("/orgs/<hhb_id:int>")
@bottle.get("/orgs/<hhb_id:int>.json")
@bottle.get("/orgs/<hhb_id:int>.html")
@autoview("details_organization")
def get_detail_organization(normdb, hhb_id):
    return render_details(normdb, "Organization", hhb_id, {
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "node_id": __.id(),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),

        "labels": LabelsSnippet(__),  # 1-n

        "types": TplDictList(__.out("used_types"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type"))  # 1-1
            }),

        "names": TplDictList(__.out("used_names"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "name": __.out("name").values("text"),  # 1-1
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "lang": __.out("name").values("lang")  # 1-1
            }),

        "relations_to_place": TplDictList(__.out("relations_to_place"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "place": IDandLabelsSnippet(__.out("other_place")),  # 1-1
            }),
        })


@bottle.get("/families/<hhb_id:int>")
@bottle.get("/families/<hhb_id:int>.json")
@bottle.get("/families/<hhb_id:int>.html")
@autoview("details_family")
def get_detail_family(normdb, hhb_id):
    return render_details(normdb, "Family", hhb_id, {
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "node_id": __.id(),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),

        "labels": LabelsSnippet(__),  # 1-n

        "types": TplDictList(__.out("used_types"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type"))  # 1-1
            }),

        "names": TplDictList(__.out("used_names"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "name": FamilyNameSnippet(__.out("name")),  # 1-1
            "type": ThesaurusConceptSnippet(__.out("type"))  # 1-1
            }),

        "relations_to_place": TplDictList(__.out("relations_to_place"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "place": IDandLabelsSnippet(__.out("other_place")),  # 1-1
            }),
        })


@bottle.get("/concepts/<hhb_id:int>")
@bottle.get("/concepts/<hhb_id:int>.json")
@bottle.get("/concepts/<hhb_id:int>.html")
@autoview("details_concept")
def get_detail_concept(normdb, hhb_id):
    def _format_labels(seq):
        res = list(map(dict, seq))
        for dct in res:
            dct["is_preferred"] = strtobool(dct["is_preferred"])
        return res

    return render_details(normdb, "Concept", hhb_id, {
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "node_id": __.id(),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),

        "labels": TplCast(  # 1-n
            _format_labels, TplList(__.out("labels").value_map())),

        "definitions": TplDictList(__.out("definitions"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "lang": __.out("definition").values("lang"),  # 1-1
            "text": __.out("definition").values("text"),  # 1-1
            }),

        "relations_to_concept": TplDictList(__.out("related_concepts"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "relation": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "concept": IDandLabelsSnippet(__.out("other_concept")),  # 1-1
            }),
        })


@bottle.get("/lemmas/<hhb_id:int>")
@bottle.get("/lemmas/<hhb_id:int>.json")
@bottle.get("/lemmas/<hhb_id:int>.html")
# @autoview("details_lemma")
def get_detail_lemma(normdb, hhb_id):
    return render_details(normdb, "Lemma", hhb_id, {
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "node_id": __.id(),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),

        "labels": LabelsSnippet(__),  # 1-n
        # "std_variants": ValueMapList(__.out("standard_variants").value_map("text", "lang")),  # 1-n
        # "semantic_entries": TplDictList(__.out("meanings"), {
        #     "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        #     "license": LicenseSnippet(__.identity()),  # 1-1
        #     "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
        #         "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
        #         "institution": __.out("institution").values("name")  # 1-1
        #         }),
        #     "lemma_variants": TplDictList(__.out("lemma_variants"), {  # 0-n
        #         # "attributions": None,
        #         "text": __.values("text"),  # 1-1
        #         "type": IsStandardSnippet(__.identity()),  # 1-1
        #         "morphology": {
        #             "gender": TplDictList(__.out("gender"), ThesaurusConceptSnippet(__.identity())),  # 0-n
        #             "number": TplDictList(__.out("number"), ThesaurusConceptSnippet(__.identity())),  # 0-2
        #             "part_of_speech": ThesaurusConceptSnippet(__.out("part_of_speech")),  # 0-1
        #             }
        #         }),
        #     "definitions": TplDictList(__.out("definitions"), {
        #         "attributions": AttributionsSnippet(__.identity()),  # 1-n
        #         "lang": __.out("definition").values("lang"),  # 1-1
        #         "text": __.out("definition").values("text"),  # 1-1
        #         }),
        #     "submeaning": {
        #         # "attributions": None,
        #         # "meaning": None,
        #         },
        #     "existences": TplDictList(  # 0-n
        #         __.out("existences"), ExistenceSnippet(__.identity())),
        #     "reference_concepts": TplDictList(__.out("reference_concepts"), {  # 0-n
        #         # "attributions": None,
        #         "concept": {
        #             **IDandLabelsSnippet(__.identity()),
        #             "uri": TplCast(
        #                 lambda id: "https://data.histhub.ch/concept/%u" % (int(id)),
        #                 __.values("hhb_id")),  # 1-1
        #             }
        #         }),
        #     }),
        # "related_lemmas": TplDictList(__.out("related_lemmas"), {  # 0-n
        #     "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        #     "uri": TplCast(
        #         lambda id: "https://data.histhub.ch/lemma/%u" % (int(id)),
        #         __.values("hhb_id")),  # 1-1
        #     })
        })
