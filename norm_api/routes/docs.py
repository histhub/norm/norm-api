import bottle
import os

from urllib.parse import urlunsplit

import norm_api

DOCS_STATIC_ROOT = os.path.join(os.path.abspath(os.path.join(
    bottle.PROJECT_DIR, "docs", "static")), "")


@bottle.get("/openapi.yaml")
@bottle.view("root_openapi.yaml")
def get_openapi_yaml(normdb):
    bottle.response.content_type = "text/yaml; charset=UTF-8"

    # Return a dictionary of variables to be used in template
    return {
        "base_url": ("%s://%s%s" % (*bottle.request.urlparts[:2], bottle.request.script_name)),
        **vars(norm_api)
        }


@bottle.get("/docs")
def get_docs_redirect(normdb):
    urlparts = list(bottle.request.urlparts)
    urlparts[2] += "/"
    return bottle.redirect(urlunsplit(urlparts))


@bottle.get("/docs/")
@bottle.get("/docs/<path:path>")
def get_docs_static(normdb, path="index.html"):
    abs_path = os.path.abspath(os.path.join(
        DOCS_STATIC_ROOT, path.strip("/\\")))
    rel_path = os.path.relpath(abs_path, start=DOCS_STATIC_ROOT)
    realpath = os.path.relpath(
        os.path.realpath(abs_path), start=DOCS_STATIC_ROOT)

    if rel_path != realpath:
        return bottle.redirect("%sdocs/%s" % (bottle.request.script_name, realpath))
    else:
        return bottle.static_file(path, root=DOCS_STATIC_ROOT)
