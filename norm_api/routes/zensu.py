import bottle
import itertools
import logging

from eavlib.gremlin.traversal import __

from norm_api.util.batch import (
    apply_template, TplCast, TplDictList, TplList, TplSwitch)
from norm_api.util.graph import lookup_node_id
from norm_api.util.misc import (maybeint, strtobool, url_format)

from norm_api.util.tpl_snippets import (
    AttributionsSnippet, DateSnippet, FamilyNameSnippet, HHbIDSnippet,
    IDandLabelsSnippet, LicenseSnippet, PersonNameUsageDictSnippet,
    ThesaurusConceptSnippet, ValueMapList)


def hhb_data_url_formatter(entity_name):
    def _format(hhb_id):
        return "https://data.histhub.ch/%s/%u" % (entity_name, hhb_id)
    return _format


def details_persons(graph, *node_ids):
    if not node_ids:
        return ()

    return apply_template({
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),
        "sexes": TplDictList(__.out("sexes"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type"))  # 1-1
            }),
        "names": TplDictList(__.out("used_names"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "name": PersonNameUsageDictSnippet(__),  # 1-1
            "type": ThesaurusConceptSnippet(__.out("type"))  # 1-1
            }),
        "events": TplDictList(__.out("events"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "what": ThesaurusConceptSnippet(__.out("what")),  # 1-1
            "when": DateSnippet(__.out("when")),  # 1-1
            "where": TplDictList(
                __.out("where"), IDandLabelsSnippet(__.identity())),  # 0-n
            }),
        "relations": TplDictList(__.out("relations"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "person": {  # 1-1
                **IDandLabelsSnippet(__.out("other_person")),
                "uri": TplCast(
                    hhb_data_url_formatter("person"),
                    HHbIDSnippet(__.out("other_person"))),
                },
            }),
        "relations_to_place": TplDictList(__.out("relations_to_place"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "place": {  # 1-1
                **IDandLabelsSnippet(__.out("other_place")),
                "uri": TplCast(
                    hhb_data_url_formatter("place"),
                    HHbIDSnippet(__.out("other_place"))),
                },
            }),
        "occupations": TplDictList(__.out("occupations"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "organization": {  # 0-1
                **IDandLabelsSnippet(__.out("employer")),
                "uri": TplCast(
                    hhb_data_url_formatter("omployer"),
                    HHbIDSnippet(__.out("employer"))),
                },
            "place": {  # 0-1
                **IDandLabelsSnippet(__.out("place")),
                "uri": TplCast(
                    hhb_data_url_formatter("place"),
                    HHbIDSnippet(__.out("place"))),
                },
            }),
        "titles": TplDictList(__.out("used_names").where(__.out("title")), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("title"))  # 1-1
            }),
        }, graph.traversal().v(*node_ids), omit_empty=True)


def details_places(graph, *node_ids):
    if not node_ids:
        return ()

    return apply_template({
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),
        "types": TplDictList(__.out("used_types"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "function": ThesaurusConceptSnippet(__.out("function"))  # 0-1
            }),
        "names": TplDictList(__.out("used_names"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "name": __.out("name").values("text"),  # 1-1
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "lang": __.out("name").values("lang")  # 1-1
            }),
        "localisations": TplDictList(__.out("used_locations"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "localisation": TplSwitch(__.out("location").label(), {  # 1-1
                "Geometry": TplCast(dict, __.out("location").value_map()),
                "FreeTextLocation": ValueMapList(  # 1-n
                    __.out("location").out("definitions").value_map()),
                "SpatialRelation": {
                    "type": ThesaurusConceptSnippet(  # 1-1
                        __.out("location").out("type")),
                    "place": {  # 1-1
                        **IDandLabelsSnippet(__.out("location").out("other_place")),
                        "uri": TplCast(  # 1-1
                            hhb_data_url_formatter("place"),
                            HHbIDSnippet(__.out("location").out("other_place"))),
                        },
                    },
                }),
            }),
        "events": TplDictList(__.out("events"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "what": ThesaurusConceptSnippet(__.out("what")),  # 1-1
            "when": DateSnippet(__.out("when")),  # 1-1
            "where": TplDictList(
                __.out("where"), IDandLabelsSnippet(__.identity())),  # 0-n
            }),
        }, graph.traversal().v(*node_ids), omit_empty=True)


def details_organizations(graph, *node_ids):
    if not node_ids:
        return ()

    return apply_template({
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),
        "types": TplDictList(__.out("used_types"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type"))  # 1-1
            }),
        "names": TplDictList(__.out("used_names"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "name": __.out("name").values("text"),  # 1-1
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "lang": __.out("name").values("lang")  # 1-1
            }),
        "relations_to_place": TplDictList(__.out("relations_to_place"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "place": {  # 1-1
                **IDandLabelsSnippet(__.out("other_place")),
                "uri": TplCast(  # 1-1
                    hhb_data_url_formatter("place"),
                    HHbIDSnippet(__.out("other_place"))),
                },
            }),
        }, graph.traversal().v(*node_ids), omit_empty=True)


def details_families(graph, *node_ids):
    if not node_ids:
        return ()

    return apply_template({
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),
        "types": TplDictList(__.out("used_types"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type"))  # 1-1
            }),
        "names": TplDictList(__.out("used_names"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "name": FamilyNameSnippet(__.out("name")),  # 1-1
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            }),
        "relations_to_place": TplDictList(__.out("relations_to_place"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "type": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "place": {  # 1-1
                **IDandLabelsSnippet(__.out("other_place")),
                "uri": TplCast(  # 1-1
                    hhb_data_url_formatter("place"),
                    HHbIDSnippet(__.out("other_place"))),
                },
            }),
        }, graph.traversal().v(*node_ids), omit_empty=True)


def details_concepts(graph, *node_ids):
    if not node_ids:
        return ()

    def _format_labels(seq):
        res = list(map(dict, seq))
        for dct in res:
            dct["is_preferred"] = strtobool(dct["is_preferred"])
        return res

    return apply_template({
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),
        "labels": TplCast(  # 1-n
            _format_labels, TplList(__.out("labels").value_map())),
        "definitions": TplDictList(__.out("definitions"), {  # 1-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "lang": __.out("definition").values("lang"),  # 1-1
            "text": __.out("definition").values("text"),  # 1-1
            }),
        "relations_to_concept": TplDictList(__.out("related_concepts"), {  # 0-n
            "attributions": AttributionsSnippet(__.identity()),  # 1-n
            "relation": ThesaurusConceptSnippet(__.out("type")),  # 1-1
            "concept": {  # 1-1
                **IDandLabelsSnippet(__.out("other_concept")),
                "uri": TplCast(
                    hhb_data_url_formatter("concept"),
                    HHbIDSnippet(__.out("other_concept"))),
                },
            }),
        }, graph.traversal().v(*node_ids), omit_empty=True)


def details_terms(graph, *node_ids):
    if not node_ids:
        return ()

    def _terms(vmaps):
        def conv(vmap):
            d = dict(vmap)
            if "is_preferred" in d:
                d["is_preferred"] = strtobool(d["is_preferred"])
            return d
        return tuple(map(conv, vmaps))

    def _vocabulary(vmap):
        return {"hhb_id": int(vmap["hhb_id"]), "name": vmap["name"]} if vmap else None

    return apply_template({
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),
        "vocabulary": TplCast(_vocabulary, __.in_("root").value_map("hhb_id", "name")),  # 0-1
        "parent": HHbIDSnippet(__.out("parent")),
        "terms": TplCast(_terms, TplList(__.out("terms").value_map())),  # 1-n
        "definitions": ValueMapList(__.out("definitions").value_map("lang", "text")),  # 0-n
        }, graph.traversal().v(*node_ids), omit_empty=True)


def details_lemmas(graph, *node_ids):
    if not node_ids:
        return ()

    return apply_template({
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "license": LicenseSnippet(__.identity()),  # 1-1
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            "institution": __.out("institution").values("name")  # 1-1
            }),
        # "std_variants": ValueMapList(__.out("standard_variants").value_map("text", "lang")),  # 1-n
        # "semantic_entries": TplDictList(__.out("meanings"), {
        #     "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        #     "license": LicenseSnippet(__.identity()),  # 1-1
        #     "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
        #         "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
        #         "institution": __.out("institution").values("name")  # 1-1
        #         }),
        #     "lemma_variants": TplDictList(__.out("lemma_variants"), {  # 0-n
        #         # "attributions": None,
        #         "text": __.values("text"),  # 1-1
        #         "type": IsStandardSnippet(__.identity()),  # 1-1
        #         "morphology": {
        #             "gender": TplDictList(__.out("gender"), ThesaurusConceptSnippet(__.identity())),  # 0-n
        #             "number": TplDictList(__.out("number"), ThesaurusConceptSnippet(__.identity())),  # 0-2
        #             "part_of_speech": ThesaurusConceptSnippet(__.out("part_of_speech")),  # 0-1
        #             }
        #         }),
        #     "definitions": TplDictList(__.out("definitions"), {
        #         "attributions": AttributionsSnippet(__.identity()),  # 1-n
        #         "lang": __.out("definition").values("lang"),  # 1-1
        #         "text": __.out("definition").values("text"),  # 1-1
        #         }),
        #     "submeaning": {
        #         # "attributions": None,
        #         # "meaning": None,
        #         },
        #     "existences": TplDictList(  # 0-n
        #         __.out("existences"), ExistenceSnippet(__.identity())),
        #     "reference_concepts": TplDictList(__.out("reference_concepts"), {  # 0-n
        #         # "attributions": None,
        #         "concept": {
        #             **IDandLabelsSnippet(__.identity()),
        #             "uri": TplCast(
        #                 lambda id: "https://data.histhub.ch/concept/%u" % (int(id)),
        #                 __.values("hhb_id")),  # 1-1
        #             }
        #         }),
        #     }),
        # "related_lemmas": TplDictList(__.out("related_lemmas"), {  # 0-n
        #     "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        #     "uri": TplCast(
        #         lambda id: "https://data.histhub.ch/lemma/%u" % (int(id)),
        #         __.values("hhb_id")),  # 1-1
        #     })
        }, graph.traversal().v(*node_ids), omit_empty=True)


def check_auth(user, password):
    from norm_api.app import app
    conf_username = app.config.get("zensu.username")
    conf_password = app.config.get("zensu.password")

    if not conf_username or conf_password is None:
        logging.warning("No auth credentials for /zensu is configured! "
                        "Blocking all requests.")
        return False

    return user == conf_username and password == conf_password


def sync_entities(normdb, entity_name, details_func):
    limit = int(bottle.request.query.get("limit", 100))
    offset = int(bottle.request.query.get("offset", 0))

    if limit <= 0:
        raise bottle.HTTPError(400, "limit must be greater than zero")
    if offset < 0:
        raise bottle.HTTPError(400, "offset must not be negative")

    graph = normdb.graph
    all_entities_tr = graph.traversal().v().has_label(entity_name)
    selection = all_entities_tr.order().by("hhb_id", "ASC").range(
        offset, (offset + limit + 1)).id()
    results = details_func(graph, *itertools.islice(selection, limit))
    count = len(results)

    prev_url = next_url = None
    if offset > 0:
        if count < 1:
            # past the end of the result set (set the prev_url to the end)
            total = int(next(all_entities_tr.count()))
            prev_url = url_format(query={
                "offset": (total - (total % limit)), "limit": limit})
        else:
            prev_url = url_format(query={
                "offset": max(0, (offset - limit)),
                "limit": min(offset, limit)})
    if next(selection, None) is not None:
        next_url = url_format(query={
            "offset": (offset + limit), "limit": limit})

    return {"count": count, "prev": prev_url, "next": next_url,
            "results": results}


@bottle.get("/zensu/persons")
@bottle.auth_basic(check=check_auth)
def sync_persons(normdb):
    return sync_entities(normdb, "Person", details_persons)


@bottle.get("/zensu/places")
@bottle.auth_basic(check=check_auth)
def sync_places(normdb):
    return sync_entities(normdb, "Place", details_places)


# XXX: orgs is legacy
@bottle.get("/zensu/organizations")
@bottle.get("/zensu/orgs")
@bottle.auth_basic(check=check_auth)
def sync_organizations(normdb):
    return sync_entities(normdb, "Organization", details_organizations)


@bottle.get("/zensu/families")
@bottle.auth_basic(check=check_auth)
def sync_families(normdb):
    return sync_entities(normdb, "Family", details_families)


@bottle.get("/zensu/concepts")
@bottle.auth_basic(check=check_auth)
def sync_concepts(normdb):
    return sync_entities(normdb, "Concept", details_concepts)


@bottle.get("/zensu/terms")
@bottle.auth_basic(check=check_auth)
def sync_terms(normdb):
    return sync_entities(normdb, "ThesaurusConcept", details_terms)


@bottle.get("/zensu/lemmas")
@bottle.auth_basic(check=check_auth)
def sync_lemmas(normdb):
    return sync_entities(normdb, "Lemma", details_lemmas)


def get_entity(details_func, graph, label, hhb_id):
    try:
        node_id = lookup_node_id(graph, label, hhb_id)
    except StopIteration:
        raise bottle.HTTPError(404, "No such entity")
    return details_func(graph, node_id)[0]


@bottle.get("/zensu/persons/<hhb_id:int>")
@bottle.get("/zensu/persons/<hhb_id:int>.json")
@bottle.auth_basic(check=check_auth)
def get_person(normdb, hhb_id):
    return get_entity(details_persons, normdb.graph, "Person", hhb_id)


@bottle.get("/zensu/places/<hhb_id:int>")
@bottle.get("/zensu/places/<hhb_id:int>.json")
@bottle.auth_basic(check=check_auth)
def get_place(normdb, hhb_id):
    return get_entity(details_places, normdb.graph, "Place", hhb_id)


# XXX: orgs is legacy
@bottle.get("/zensu/organizations/<hhb_id:int>")
@bottle.get("/zensu/organizations/<hhb_id:int>.json")
@bottle.get("/zensu/orgs/<hhb_id:int>")
@bottle.get("/zensu/orgs/<hhb_id:int>.json")
@bottle.auth_basic(check=check_auth)
def get_organization(normdb, hhb_id):
    return get_entity(
        details_organizations, normdb.graph, "Organization", hhb_id)


@bottle.get("/zensu/families/<hhb_id:int>")
@bottle.get("/zensu/families/<hhb_id:int>.json")
@bottle.auth_basic(check=check_auth)
def get_family(normdb, hhb_id):
    return get_entity(
        details_families, normdb.graph, "Family", hhb_id)


@bottle.get("/zensu/concepts/<hhb_id:int>")
@bottle.get("/zensu/concepts/<hhb_id:int>.json")
@bottle.auth_basic(check=check_auth)
def get_concept(normdb, hhb_id):
    return get_entity(details_concepts, normdb.graph, "Concept", hhb_id)


@bottle.get("/zensu/terms/<hhb_id:int>")
@bottle.get("/zensu/terms/<hhb_id:int>.json")
@bottle.auth_basic(check=check_auth)
def get_term(normdb, hhb_id):
    return get_entity(details_terms, normdb.graph, "ThesaurusConcept", hhb_id)


@bottle.get("/zensu/lemmas/<hhb_id:int>")
@bottle.get("/zensu/lemmas/<hhb_id:int>.json")
@bottle.auth_basic(check=check_auth)
def get_lemma(normdb, hhb_id):
    return get_entity(details_lemmas, normdb.graph, "Lemma", hhb_id)
