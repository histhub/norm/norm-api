import bottle
import itertools
import re

from eavlib.gremlin.traversal import (__, TextP)

from norm_api.util.tpl_snippets import (HHbIDSnippet, LabelsSnippet)
from norm_api.util.batch import (apply_template, TplCast, TplDictList)
from norm_api.util.misc import (maybeint, url_format)
from norm_api.util.route import autoview


class Filters:
    def __init__(self, label, graph):
        self._label = label
        self._graph = graph

    def external_id(self, val, selection):
        val_match = re.match("^(([A-Za-z0-9_.-]+):)?([A-Za-z0-9.-]+)$", val)
        if val_match is None:
            raise ValueError("value is invalid")

        provider_name = val_match.group(2)
        provider_id = val_match.group(3)

        g = self._graph.traversal()

        t = g.v(*selection) if selection else g.v()
        t = t.has("external_id", provider_id)
        if provider_name:
            t = t.where(
                __.out("institution").has("name", provider_name))
        t = t.in_("external_ids").has_label(self._label)

        res = tuple(t)
        if len(res) > 1:
            raise ValueError("more than one node with same external ID")
        return res

    def label(self, val, selection):
        f = __.out("labels")
        parts = val.split(":")
        if not parts:
            raise ValueError("value is invalid")
        f = f.has("text", TextP.containing(parts.pop()))
        try:
            f = f.has("lang", parts.pop())
        except IndexError:
            pass

        return tuple(self._graph.traversal().v(*(
            selection or ())).has_label(self._label).where(f))


def entity_abstracts(graph, label, nodes, omit_empty=True):
    if not nodes:
        return ()

    g = graph.traversal()

    tpl = {
        "hhb_id": HHbIDSnippet(__.identity()),  # 1-1
        "href": TplCast(  # 1-1
            lambda hhb_id: url_format(path=("%u.json" % (int(hhb_id)))),
            __.values("hhb_id")),
        "labels": LabelsSnippet(__),  # 1-n
        "external_ids": TplDictList(__.out("external_ids"), {  # 1-n
            "institution": __.out("institution").values("name"),  # 1-1
            "external_id": TplCast(maybeint, __.values("external_id")),  # 1-1
            }),
        }

    # if label == "Person":
    #     tpl.update({
    #         "names": TplDictList(lambda nodes: g.v(*nodes).out("used_names"), {  # required, 1-to-n
    #             "providers": ProvidersSnippet(lambda nodes: g.v(*nodes).out("provider")),  # optional, 0-to-n
    #             "fullname": PersonNameUsageDictSnippet(lambda nodes: g.v(*nodes))["fullname"],  # 1-to-1, empty string if not present
    #             "lang": lambda nodes: g.v(*nodes).out("name").values("lang"),  # required
    #             "type": IsStandardSnippet(lambda nodes: g.v(*nodes).out("name"))  # required
    #             }),
    #         "titles": TplDictList(
    #             lambda ids: g.v(*ids).out("used_names").where(__.out("type")), {  # required, 1-to-n; TODO: Consider removing out("type") as it’s required
    #                 "providers": ProvidersSnippet(lambda ids: g.v(*ids).out("provider")),  # optional, 0-to-n
    #                 "term": ConceptSnippet(lambda ids: g.v(*ids).out("type"))  # required, 1-to-1
    #             }),
    #         })
    # elif label == "Place":
    #     tpl.update({
    #         "types": TplDictList(lambda ids: g.v(*ids).out("used_types"), {  # 0-to-n
    #             "providers": ProvidersSnippet(lambda ids: g.v(*ids).out("provider")),  # optional, 0-to-n
    #             "type": ConceptSnippet(lambda ids: g.v(*ids).out("type")),  # required, 1-to-1
    #             "function": ConceptSnippet(lambda ids: g.v(*ids).out("function"))  # optional, 0-to-1
    #             }),
    #         "names": TplDictList(lambda ids: g.v(*ids).out("used_names"), {  # required, 1-to-n
    #             "providers": ProvidersSnippet(lambda ids: g.v(*ids).out("provider")),  # optional, 0-to-n
    #             "name": lambda ids: g.v(*ids).out("name").out("appellation").values("spelling"),  # required, 1-to-1; FIXME: **invalid**. name does not necessarily have to have appellation
    #             "type": IsStandardSnippet(lambda ids: g.v(*ids).out("name")),  # required, 1-to-1
    #             "lang": lambda nodes: g.v(*nodes).out("name").out("appellation").values("lang")  # required, 1-to-1; FIXME: **invalid**. name does not necessarily have to have appellation
    #             }),
    #         })
    # elif label == "Organization":
    #     tpl.update({
    #         "types": TplDictList(lambda ids: g.v(*ids).out("used_types"), {  # required, 1-to-n
    #             "providers": ProvidersSnippet(lambda ids: g.v(*ids).out("provider")),  # optional, 0-to-n
    #             "type": ConceptSnippet(lambda ids: g.v(*ids).out("type"))  # required, 1-to-1
    #             }),
    #         "names": TplDictList(lambda ids: g.v(*ids).out("used_names"), {  # required, 1-to-n
    #             "providers": ProvidersSnippet(lambda ids: g.v(*ids).out("provider")),  # optional, 0-to-n
    #             "name": lambda ids: g.v(*ids).out("name").out("appellation").values("spelling"),  # required, 1-to-1; FIXME: **invalid**. name does not necessarily have to have appellation
    #             "type": IsStandardSnippet(lambda ids: g.v(*ids).out("name")),  # required, 1-to-1
    #             "lang": lambda nodes: g.v(*nodes).out("name").out("appellation").values("lang")  # required, 1-to-1; FIXME: **invalid**. name does not necessarily have to have appellation
    #             }),
    #         })
    # elif label in ("Concept"):
    #     tpl.update({
    #         "labels": ConceptSnippet(lambda nodes: g.v(*nodes))["labels"]  # required, "should" be empty dict if no labels
    #         })

    return apply_template(tpl, g.v(*nodes), omit_empty=omit_empty)


def search_handler(label, normdb):
    limit = int(bottle.request.query.pop("limit", 100))
    offset = int(bottle.request.query.pop("offset", 0))

    if limit <= 0:
        raise bottle.HTTPError(400, "limit must be greater than zero")
    if offset < 0:
        raise bottle.HTTPError(400, "offset must not be negative")

    graph = normdb.graph
    filters = Filters(label, graph)
    selection = None
    query = bottle.request.query.decode()
    if query:
        # Filter the result
        for f, val in query.items():
            # val = raw_val.decode(query.input_encoding)
            try:
                filter_func = getattr(filters, f)
                if not callable(filter_func):
                    raise AttributeError("filter not callable")
            except AttributeError as e:
                raise bottle.HTTPError(
                    400, "Query parameter '%s' is invalid!" % f) from e
            else:
                try:
                    selection = filter_func(val, selection)
                except bottle.HTTPError:
                    raise
                except Exception as e:
                    raise bottle.HTTPError(500, "Filter %s failed: %s" % (f, e)) from e

        # Skip the offset. Limit filtering will be done later
        selection = itertools.islice(selection, offset, None)
    else:
        # All of it (we load one more to determine if there are more results)
        selection = graph.traversal().v().has_label(label).order().by(
            "hhb_id", "ASC").range(offset, (offset + limit + 1))

    results = tuple(itertools.islice(selection, limit))
    count = len(results)

    if count < 1:
        raise bottle.HTTPError(404, (
            "no results matching these criteria"
            if len(bottle.request.query) else "no entities found"))

    prev_url = next_url = None
    if offset > 0:
        prev_url = url_format(query={
            "offset": max(0, (offset - limit)),
            "limit": min(offset, limit)
            })

    if next(selection, None) is not None:
        next_url = url_format(query={
            "offset": (offset + limit),
            "limit": limit
            })

    return dict(count=count, offset=offset, prev=prev_url, next=next_url,
                results=entity_abstracts(graph, label, results))


@bottle.get("/persons")
@autoview("collection", h1="Personen")
def get_persons(normdb):
    return search_handler("Person", normdb)


@bottle.get("/places")
@autoview("collection", h1="Orte")
def get_places(normdb):
    return search_handler("Place", normdb)


# XXX: orgs is legacy
@bottle.get("/orgs")
@bottle.get("/organizations")
@autoview("collection", h1="Organisationen")
def get_organizations(normdb):
    return search_handler("Organization", normdb)


@bottle.get("/families")
@autoview("collection", h1="Familien")
def get_families(normdb):
    return search_handler("Family", normdb)


@bottle.get("/concepts")
@autoview("collection", h1="Konzepte")
def get_concepts(normdb):
    return search_handler("Concept", normdb)


@bottle.get("/lemmas")
@autoview("collection", h1="Lemmas")
def get_lemmas(normdb):
    return search_handler("Lemma", normdb)
