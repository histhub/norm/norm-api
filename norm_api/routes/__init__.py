# NOTE: noqa to suppress "unused imports" warnings
# flake8: noqa

import bottle

from . import (collection, detail, docs, graph, search, zensu)


@bottle.get("/")
def get_root(normdb):
    return bottle.template("root", normdb=normdb)
