import bottle

from normlib.dump import DeepNodeSerialiser
from normlib.db.dump_manager import DumpManager

from norm_api.util.misc import (url_format)


def nodes_formatter(node_id):
    return url_format(path=("/nodes/%u" % node_id))


@bottle.get("/nodes/<node_id:int>")
@bottle.get("/nodes/<node_id:int>.json")
def get_graph_node(normdb, node_id):
    hidden = bool(bottle.request.query.get("hidden", 0))
    max_depth = int(bottle.request.query.get("max_depth", 20))

    dump_manager = DumpManager(normdb)
    try:
        deep_node = dump_manager.dump_object(int(node_id), max_depth=max_depth)
    except (TypeError, ValueError) as e:
        raise bottle.HTTPError(400, e)
    else:
        if not deep_node:
            raise bottle.HTTPError(404, "No such node")

    return DeepNodeSerialiser.as_dict(
        deep_node, enable_hidden=hidden, href_formatter=nodes_formatter)


@bottle.get("/nodes/<node_id:int>.xml")
def get_graph_node_xml(normdb, node_id):
    hidden = bool(bottle.request.query.get("hidden", 0))
    max_depth = int(bottle.request.query.get("max_depth", 20))

    dump_manager = DumpManager(normdb)
    try:
        deep_node = dump_manager.dump_object(int(node_id), max_depth=max_depth)
    except (TypeError, ValueError) as e:
        raise bottle.HTTPError(400, e)
    else:
        if not deep_node:
            raise bottle.HTTPError(404, "No such node")

    bottle.response.content_type = "application/xml; charset=utf-8"
    return DeepNodeSerialiser.as_xml(
        deep_node, enable_hidden=hidden, href_formatter=nodes_formatter)
