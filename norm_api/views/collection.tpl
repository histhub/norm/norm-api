%import re
%from norm_api.util.misc import select_label
%lang = locals().get("lang", "deu")
<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="utf-8"/>
	</head>
	<body>
		<a href="/">« Startseite</a>

		<h1>{{ locals().get("h1", "Entitäten") }}</h1>

		Zeige {{count}} Suchresultate.

		<table border="1">
			<thead>
				<th>#</th>
				<th>histHub-ID</th>
				<th>Label</th>
				<th>Formate</th>
			</thead>
			<tbody>
				%for i_, res_ in enumerate(results, locals().get("offset", 0) + 1):
				<tr>
					<td>{{ i_ }}</td>
					<td>{{ res_["hhb_id"] }}</td>
					<td>{{ select_label(res_["labels"], lang) }}</td>
					<td>
						%for fmt_ in ("html", "json"):
						<a href="{{ re.sub(r"\.[a-z]*$", ("." + fmt_), res_["href"]) }}">{{ fmt_.upper() }}</a>
						%end
					</td>
				</tr>
				%end
			</tbody>
		</table>

		<div>
			%if prev:
			<a href="{{ prev }}">« Vorherige Seite</a>
			%end
			%if next:
			<a href="{{ next }}">» N&auml;chste Seite</a>
			%end
		</div>
	</body>
</html>
