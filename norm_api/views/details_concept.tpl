%from norm_api.util.misc import (AttributionsManager, ExtURLMaker, format_volume_reference, select_label)
%lang = locals().get("lang", "deu")
%attributions = AttributionsManager()
<!DOCTYPE html>
<html lang="{{ lang }}">
	<head>
		<title>Konzept ({{ hhb_id }})</title>
		<meta charset="utf-8"/>
	</head>
	<body>
		<h1>Konzept ({{ hhb_id }})</h1>

		<p>
			<b>histHub ID:</b> {{ hhb_id }}<br/>
			<b>Node ID (intern):</b> <a href="/nodes/{{ node_id }}.xml">{{ node_id }}</a><br/>
		</p>
		<p>
			<b>Externe IDs:</b>
			<ul>
				%for extid_ in external_ids:
				<li>
					{{ extid_["institution"] }}:
					%url_ = ExtURLMaker.make(extid_["institution"], extid_["external_id"])
					%if url_:
					<a href="{{ url_ }}" target="_blank">{{ extid_["external_id"] }}</a>
					%else:
					{{ extid_["external_id"] }}
					%end
				</li>
				%end
			</ul>
		</p>
		%if locals().get("labels", None):
		<p>
			<b>Labels:</b>
			<ul>
				%for label_ in labels:
				<li>{{ label_["text"] }} [{{ label_["lang"] }}, {{ "bevorzugt" if label_["is_preferred"] else "" }}]</li>
				%end
			</ul>
		</p>
		%end
		%if locals().get("definitions", None):
		<p>
			<b>Definitionen:</b>
			<dl>
				%for def_ in definitions:
				<dt>{{ def_["lang"] }}</dt>
				<dd>
					{{ def_["text"] }}
					%for attrib_ in def_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</dd>
				%end
			</dl>
		</p>
		%end

		<hr align="left" color="black" size="1" width="200"/>
		<ol>
			%for i_, attrib_ in attributions.footnotes():
			<li value="{{ i_ }}">
				%if "caption" in attrib_:
				{{! render_commonmark(attrib_["caption"]) }}
				%else:
				{{ attrib_["institution"] }}
				%if "revision" in attrib_:
				; Revision: attrib_["revision"]
				%end
				%if "reference" in attrib_ and attrib_["reference"]["volume"]:
				&ndash; {{ format_volume_reference(attrib_["reference"]) }}
				%end
				%end
			</li>
			%end
		</ol>
	</body>
</html>
