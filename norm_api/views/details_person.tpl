%from norm_api.util.misc import (AttributionsManager, ExtURLMaker, HHbDateFormatter, format_volume_reference, select_label)
%lang = locals().get("lang", "deu")
%attributions = AttributionsManager()
<!DOCTYPE html>
<html lang="{{ lang }}">
	<head>
		<title>Person "{{ select_label(labels, lang) }}" ({{ hhb_id }})</title>
		<meta charset="utf-8"/>
	</head>
	<body>
		<h1>Person "{{ select_label(labels, lang) }}" ({{ hhb_id }})</h1>

		<p>
			<b>histHub ID:</b> {{hhb_id}}<br/>
			<b>Node ID (intern):</b> <a href="/nodes/{{ node_id }}.xml">{{ node_id }}</a><br/>
		</p>
		<p>
			<b>Externe IDs:</b>
			<ul>
				%for extid_ in external_ids:
				<li>
					{{ extid_["institution"] }}:
					%url_ = ExtURLMaker.make(extid_["institution"], extid_["external_id"])
					%if url_:
					<a href="{{ url_ }}" target="_blank">{{ extid_["external_id"] }}</a>
					%else:
					{{ extid_["external_id"] }}
					%end
				</li>
				%end
			</ul>
		</p>
		%if locals().get("names", None):
		<p>
			<b>Namen:</b>
			<ul>
				%for name_ in names:
				<li>
					{{name_["name"]["fullname"]}} [{{name_["type"]["terms"][lang]}}]
					%for attrib_ in name_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end
			</ul>
		</p>
		%end
		%if locals().get("titles", None):
		<p>
			<b>Titel:</b>
			<ul>
				%for title_ in titles:
				<li>
					{{ title_["type"]["terms"][lang] }}
					%for attrib_ in title_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end
			</ul>
		</p>
		%end
		%if locals().get("sexes", None):
		<p>
			<b>Geschlecht:</b>
			<ul>
				%for sex_ in sexes:
				<li>
					{{ sex_["type"]["terms"][lang] }}
					%for attrib_ in sex_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end
			</ul>
		</p>
		%end
		%if locals().get("events", None):
		<p>
			<b>Ereignisse:</b>
			<ul>
				%for evt_ in events:
				<li>
					%if evt_.get("what", None):
					{{ evt_["what"]["terms"][lang] }}:
					%end
					%if evt_.get("when", None):
					{{ HHbDateFormatter.format(evt_["when"]) }}
					%end
					%if evt_.get("where", None):
					in {{ select_label(occupation_["place"]["labels"], lang) }} (<a href="/places/{{ occupation_["place"]["id"] }}">{{ occupation_["place"]["id"] }}</a>)
					%end
					%for attrib_ in evt_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end
			</ul>
		</p>
		%end
		%if locals().get("occupations", None):
		<p>
			<b>Besch&auml;ftigungen:</b>
			<ul>
				%for occupation_ in occupations:
				<li>
					<b>{{ occupation_["type"]["terms"][lang] }}</b>
					%if occupation_.get("organization", None):
					von {{ select_label(occupation_["organization"]["labels"], lang) }} (<a href="/organizations/{{ occupation_["organization"]["id"] }}">{{ occupation_["organization"]["id"] }}</a>)
					%end
					%if occupation_.get("place", None):
					in {{ select_label(occupation_["place"]["labels"], lang) }}
					%end
					%for attrib_ in occupation_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end
			</ul>
		</p>
		%end
		%if locals().get("relations", None):
		<p>
			<b>Beziehungen:</b>
			<ul>
				%for rel_ in relations:
				<li>
					<b>{{ rel_["type"]["terms"][lang] }}</b>
					{{ select_label(rel_["person"]["labels"], lang) }} (<a href="/persons/{{ rel_["person"]["id"] }}">{{ rel_["person"]["id"] }}</a>)
					%for attrib_ in rel_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end
			</ul>
		</p>
		%end
		%if locals().get("relations_to_place", None):
		<p>
			<b>Ortsbeziehungen:</b>
			<ul>
				%for r2pl_ in relations_to_place:
				<li>
					<b>{{ select_label(r2pl_["type"]["terms"], lang) }}:</b>
					{{ select_label(r2pl_["place"]["labels"], lang) }} (<a href="/places/{{ r2pl_["place"]["id"] }}.html">{{ r2pl_["place"]["id"] }}</a>)
					%for attrib_ in r2pl_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end
			</ul>
		</p>
		%end

		<hr align="left" color="black" size="1" width="200"/>
		<ol>
			%for i_, attrib_ in attributions.footnotes():
			<li value="{{ i_ }}">
				%if "caption" in attrib_:
				{{! render_commonmark(attrib_["caption"]) }}
				%else:
				{{ attrib_["institution"] }}
				%if "revision" in attrib_:
				; Revision: attrib_["revision"]
				%end
				%if "reference" in attrib_ and attrib_["reference"]["volume"]:
				&ndash; {{ format_volume_reference(attrib_["reference"]) }}
				%end
				%end
			</li>
			%end
		</ol>
	</body>
</html>
