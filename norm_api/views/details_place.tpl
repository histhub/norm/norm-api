%from norm_api.util.misc import (AttributionsManager, ExtURLMaker, GeometryFormatter, HHbDateFormatter, format_volume_reference, render_commonmark, select_label)
%lang = locals().get("lang", "deu")
%attributions = AttributionsManager()
<!DOCTYPE html>
<html>
	<head>
		<title>Ort "{{ select_label(labels, lang) }}" ({{ hhb_id }})</title>
		<meta charset="utf-8"/>
	</head>
	<body>
		<h1>Ort "{{ select_label(labels, lang) }}" ({{ hhb_id }})</h1>

		<p>
			<b>histHub ID:</b> {{hhb_id}}<br/>
			<b>Node ID (intern):</b> <a href="/nodes/{{node_id}}.xml">{{node_id}}</a>
		</p>
		<p>
			<b>Externe IDs:</b>
			<ul>
				%for extid_ in external_ids:
				<li>
					{{ extid_["institution"] }}:
					%url_ = ExtURLMaker.make(extid_["institution"], extid_["external_id"])
					%if url_:
					<a href="{{ url_ }}" target="_blank">{{ extid_["external_id"] }}</a>
					%else:
					{{ extid_["external_id"] }}
					%end
				</li>
				%end
			</ul>
		</p>
		%if locals().get("names", None):
		<p>
			<b>Namen:</b>
			<ul>
				%for name_ in names:
				<li>
					<b>{{name_["name"]}}</b>
					[{{name_["lang"]}}, {{name_["type"]["terms"][lang]}}]
					%for attrib_ in name_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end
			</ul>
		</p>
		%end
		%if locals().get("types", None):
		<p>
			<b>Ortstypen:</b>
			<ul>
				%for type_ in types:
				<li>
					{{type_["type"]["terms"][lang]}}
					%for attrib_ in type_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
					%if "periods" in type_:
					<ul>
						%for period_ in type_["periods"]:
						%if "start_of_period" in period_:
						<li>von: {{date_formatter.format(period_["start_of_period"])}}</li>
						%end
						%if "end_of_period" in period_:
						<li>bis: {{date_formatter.format(period_["end_of_period"])}}</li>
						%end
						%end #for
					</ul>
					%end
				</li>
				%end
			</ul>
		</p>
		%end
		%if locals().get("localisations", None):
		<p>
			<b>Lokalisierung:</b>
			<ul>
				%for loc_ in localisations:
				<li>
					%if "coordinates" in loc_["localisation"]: # Coordinates
					{{ GeometryFormatter.format(int(loc_["localisation"]["srid"]), loc_["localisation"]["coordinates"]) }}
					%elif "place" in loc_["localisation"]: # SpatialRelation
					{{ select_label(loc_["localisation"]["type"]["terms"], lang) }}
					<b>{{ select_label(loc_["localisation"]["place"]["labels"], lang, default="?") }}</b>
					(<a href="/places/{{ loc_["localisation"]["place"]["id"] }}">{{ loc_["localisation"]["place"]["id"] }}</a>)
					% else:
					% location_in_lang = select_label(loc_["localisation"], lang)
					% if location_in_lang:
					{{ location_in_lang }}
					% else:
					<i>Unbekannte Lokalisierung</i>
					%end
					%end # if
					%for attrib_ in loc_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end # for
			</ul>
		</p>
		%end
		%if locals().get("events", None):
		<p>
			<b>Ereignisse:</b>
			<ul>
				%for evt_ in events:
				<li>
					%if evt_.get("what", None):
					{{ evt_["what"]["terms"][lang] }}:
					%end
					%if evt_.get("when", None):
					{{ HHbDateFormatter.format(evt_["when"]) }}
					%end
					%if evt_.get("where", None):
					in {{ select_label(evt_["place"]["labels"], lang) }} (<a href="/places/{{ evt_["place"]["id"] }}">{{ evt_["place"]["id"] }}</a>)
					%end
					%for attrib_ in evt_["attributions"]:
					<sup>{{ attributions.add(attrib_) }}</sup>
					%end
				</li>
				%end
			</ul>
		</p>
		%end

		<hr align="left" color="black" size="1" width="200"/>
		<ol>
			%for i_, attrib_ in attributions.footnotes():
			<li value="{{ i_ }}">
				%if "caption" in attrib_:
				{{! render_commonmark(attrib_["caption"]) }}
				%else:
				{{ attrib_["institution"] }}
				%if "revision" in attrib_:
				(Revision: {{ attrib_["revision"] }})
				%end
				%if "reference" in attrib_ and attrib_["reference"]["volume"]:
				&ndash; {{ format_volume_reference(attrib_["reference"]) }}
				%end
				%end
			</li>
			%end
		</ol>
	</body>
</html>
