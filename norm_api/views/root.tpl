<!DOCTYPE html>
<html lang="de">
	<head>
		<title>histHub Norm DB</title>
		<meta charset="utf-8"/>
	</head>
	<body>
		<h1>histHub Norm DB</h1>

		<form method="get" action="/go">
			<select name="entity">
				<option value="">alle</option>
				<option value="person">Person</option>
				<option value="place">Ort</option>
				<option value="organization">Organisation</option>
				<option value="family">Familie</option>
				<option value="concept">Konzept</option>
				<option value="lemma">Lemma</option>
			</select>
			<label for="go-hhb_id">histHub-ID</label>
			<input id="go-hhb_id" name="hhb_id" type="number" min="10000000"/>

			<input type="submit" value="Gehe zu"/>
		</form>

		<h2>Graphische Suche</h2>

		<form method="get" action="/search">
			<p>
				<label for="search-entity">Entit&auml;tstyp:</label><br/>
				<select id="search-entity" name="entity">
					<option value="person">Person</option>
					<option value="place">Ort</option>
					<option value="organization">Organisation</option>
					<option value="family">Familie</option>
					<option value="concept">Konzept</option>
					<option value="lemma">Lemma</option>
				</select>
			</p>

			<p>
				<label for="search-ext_institution">externe ID:</label><br/>
				<select id="search-ext_institution" name="external_institution">
					<option value=""></option>
					%for inst_ in normdb.graph.traversal().v().has("is_provider", "true").has_label("Institution").values("name"):
					<option value="{{ inst_ }}">{{ inst_ }}</option>
					%end
				</select>
				<input type="text" name="external_id"/>
			</p>

			<p>
				<label for="search-label">Label:</label><br/>
				<input type="text" id="search-label" name="label"/>
			</p>

			<p>
				<input type="submit" value="Suchen"/>
			</p>
		</form>
	</body>
</html>
