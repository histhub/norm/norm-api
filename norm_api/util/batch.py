import abc
import collections
import functools
import logging

from eavlib.gremlin.traversal import (
    __, Traversal, GraphTraversal, TraverserTuple)

from norm_api.util.graph import (eval_traversers, grouped, ordered)
from norm_api.util.misc import dict_multiget

LOG = logging.getLogger(__name__)


class TemplateProcessingError(Exception):
    pass


def exception_loc(*, name=None, pos=None):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except TemplateProcessingError:
                raise
            except Exception as e:
                if name:
                    keys = kwargs[name]
                elif pos:
                    keys = args[pos]
                else:
                    keys = "unknown location"

                raise TemplateProcessingError(
                    "An error occurred during processing of %r: %s" % (
                        keys, e)) from e
        return wrapper
    return decorator


class TplType(abc.ABC):
    _options = {
        "omit_empty": False,
        "postproc_fn": lambda x: x,
        }

    def __init__(self):
        self._options = self.__class__._options.copy()

    @property
    def options(self):
        return self._options

    @staticmethod
    def _traversal(graph, start_traversers, anon_traversal):
        ast = graph.traversal().with_traversers(start_traversers).ast
        ast.step_instructions = list(anon_traversal.ast.step_instructions)

        return GraphTraversal(graph, ast)

    @abc.abstractmethod
    def apply(self, graph, start_traversers, keys):
        pass


class TplDictList(TplType):
    def __init__(self, traversal=None, template={}):
        super().__init__()
        self.traversal = traversal if traversal is not None else __.identity()
        self.subtemplate = template

    def __repr__(self):
        return "[%r -> %r]" % (self.traversal.ast, self.subtemplate)

    @exception_loc(pos=3)
    def apply(self, graph, start_traversers, keys):
        omit_empty = self.options["omit_empty"]
        st = TraverserTuple(start_traversers)
        if not st:
            return ()

        assert all(t.value == t.path[-1] for t in st)

        res = {t.value: [] for t in st}

        # HACK: withTraversers() cannot be used because it keeps the path and
        # thus breaks our mapping.
        rtt = graph.traversal().v(*res.keys())
        rtt.ast.step_instructions += self.traversal.ast.step_instructions
        rt = TraverserTuple(eval_traversers(rtt))

        self.subtemplate.options["omit_empty"] = omit_empty

        for t, v in zip(rt, self.subtemplate.apply(graph, rt, keys)):
            if not omit_empty or v:
                res[t.path[0]].append(v)

        return tuple(tuple(res[t.value]) for t in st)


class TplList(TplType):
    def __init__(self, traversal, onempty=None):
        super().__init__()
        self.traversal = traversal
        self.onempty = onempty

    def __repr__(self):
        if self.onempty:
            return "[*(%r or %r)]" % (self.traversal.ast, self.onempty)
        else:
            return "[*%r]" % (self.traversal.ast)

    @exception_loc(pos=3)
    def apply(self, graph, start_traversers, keys):
        st = TraverserTuple(start_traversers)
        g = self._traversal(graph, st, self.traversal)
        return (x or self.onempty for x in grouped(g))


class TplCustom(TplType):
    def __init__(self, func, traversal, batch=True):
        super().__init__()
        self.traversal = traversal
        self.func = func

    @exception_loc(pos=3)
    def apply(self, graph, start_traversers, keys):
        st = TraverserTuple(start_traversers)
        g = self._traversal(graph, st, self.traversal)
        return tuple(map(self.func, ordered(g)))


class TplTraversal(TplType):
    def __init__(self, traversal):
        super().__init__()
        self.traversal = traversal

    def __repr__(self):
        return repr(self.traversal.ast)

    @exception_loc(pos=3)
    def apply(self, graph, start_traversers, keys):
        st = TraverserTuple(start_traversers)
        return ordered(self._traversal(graph, st, self.traversal))


class TplTemplate(TplType):
    def __init__(self, template={}):
        super().__init__()
        self.template = template

    def __repr__(self):
        return repr(self.template)

    @exception_loc(pos=3)
    def apply(self, graph, start_traversers, keys):
        omit_empty = self.options["omit_empty"]
        st = TraverserTuple(start_traversers)

        res = tuple({} for _ in st)

        for k, v in self.template.items():
            loc = keys + (k,)

            v.options["omit_empty"] = omit_empty

            LOG.debug("Processing keys: (%s) %r",
                      type(v), loc)

            for i, x in enumerate(v.apply(graph, st, loc)):
                if not omit_empty or x:
                    res[i][k] = x

        return tuple(x if x else None for x in res) if omit_empty else res


class TplScalar(TplType):
    def __init__(self, value):
        super().__init__()
        self.value = value

    def __repr__(self):
        return repr(self.value)

    @exception_loc(pos=3)
    def apply(self, graph, start_traversers, keys):
        return tuple(self.value for _ in start_traversers)


class TplSwitch(TplType):
    def __init__(self, switch_traversal, cases={}, default=TplScalar(None)):
        super().__init__()
        self.traversal = switch_traversal
        self.cases = cases
        self.default = default

    def __repr__(self):
        return "<%r>?%r" % (self.traversal.ast, self.cases)

    @exception_loc(pos=3)
    def apply(self, graph, start_traversers, keys):
        st = TraverserTuple(start_traversers)
        if not st:
            return ()

        for c in self.cases.values():
            c.options["omit_empty"] = self.options["omit_empty"]

        groups = collections.defaultdict(list)
        for t, v in zip(st, ordered(
                self._traversal(graph, st, self.traversal))):
            groups[v].append(t)

        res = dict.fromkeys(st)
        for v, ts in groups.items():
            subtpl = self.cases.get(v, self.default)
            res.update(zip(ts, subtpl.apply(
                graph, TraverserTuple(st.__type__, *ts), keys)))

        return dict_multiget(res, st)


class TplCast(TplType):
    def __init__(self, cast, subtemplate, skip_empty=True):
        super().__init__()
        self.cast = cast
        self.subtemplate = subtemplate
        self.skip_empty = skip_empty

    def __repr__(self):
        if isinstance(self.subtemplate, Traversal):
            # HACK
            return "%s(%r)" % (self.cast.__name__, self.subtemplate.ast)
        return "%s(%r)" % (self.cast.__name__, self.subtemplate)

    @exception_loc(pos=3)
    def apply(self, graph, start_traversers, keys):
        omit_empty = self.options["omit_empty"]
        self.subtemplate.options["omit_empty"] = omit_empty
        res = self.subtemplate.apply(graph, start_traversers, keys)

        skip_empty = self.skip_empty or omit_empty

        def caster_(x):
            if x is not None or not skip_empty:
                return self.cast(x)
            else:
                return None

        return tuple(map(caster_, res))


def compile_template(tpl):
    if isinstance(tpl, TplCast):
        return TplCast(tpl.cast, compile_template(tpl.subtemplate))
    elif isinstance(tpl, TplDictList):
        return TplDictList(tpl.traversal, compile_template(tpl.subtemplate))
    elif isinstance(tpl, TplSwitch):
        return TplSwitch(tpl.traversal, {
            k: compile_template(v) for k, v in tpl.cases.items()
            }, default=compile_template(tpl.default))
    elif isinstance(tpl, TplType):
        # probably okay??
        return tpl
    elif isinstance(tpl, dict):
        return TplTemplate({k: compile_template(v) for k, v in tpl.items()})
    elif isinstance(tpl, Traversal):
        return TplTraversal(tpl)
    elif callable(tpl):
        raise TypeError("callable traversals are not supported anymore")
    else:
        # a scalar??
        return TplScalar(tpl)


def apply_template(tpl, start_traversal, omit_empty=True):
    if not isinstance(tpl, TplType):
        tpl = compile_template(tpl)

    tpl.options["omit_empty"] = omit_empty

    graph = start_traversal.graph
    traversers = eval_traversers(start_traversal)
    return tpl.apply(graph, traversers, ())
