import bottle


class autoview:
    def __init__(self, tpl_name, **kwargs):
        self.tpl_name = tpl_name
        self.defaults = kwargs

    def __call__(self, func):
        def wrapper(*args, **kwargs):
            if bottle.request.urlparts.path.endswith(".html"):
                apply_tmpl = True
            elif bottle.request.urlparts.path.endswith(".json") \
                    or bottle.request.urlparts.path.endswith(".xml"):
                apply_tmpl = False
            else:
                apply_tmpl = False
                for acc in bottle.request.headers.get("Accept", "").split(","):
                    if "html" in acc:
                        apply_tmpl = True
                        break
                    elif "json" in acc or "xml" in acc:
                        apply_tmpl = False
                        break
            handler = func
            if apply_tmpl:
                handler = bottle.view(self.tpl_name, **self.defaults)(func)
            return handler(*args, **kwargs)

        return wrapper
