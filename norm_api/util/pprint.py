import pprint

from eavlib.gremlin.traversal import Traversal

from norm_api.util.batch import (
    TplCast, TplDictList, TplList, TplSwitch, TplTemplate, TplTraversal)


class TemplatePrettyPrinter(pprint.PrettyPrinter):
    _dispatch = pprint.PrettyPrinter._dispatch.copy()

    def _format(self, obj, stream, indent, allowance, context, level):
        objid = id(obj)
        if objid in context:
            stream.write(pprint._recursion(obj))
            self._recursive = True
            self._readable = False
            return
        p = self._dispatch.get(type(obj).__repr__, None)
        if p is not None:
            context[objid] = 1
            p(self, obj, stream, indent, allowance, context, level + 1)
            del context[objid]
            return
        stream.write(self._repr(obj, context, level))

    def _pprint_dict(self, obj, stream, indent, allowance, context, level):
        write = stream.write
        write('{\n')
        inner_indent = indent + self._indent_per_level
        write(inner_indent * ' ')
        if obj:
            items = obj.items()
            # if self._sort_dicts:
            #     items = sorted(items, key=pprint._safe_tuple)

            self._format_dict_items(
                items, stream, indent, allowance + 1, context, level)
        write('\n' + (indent * ' ') + '}')

    _dispatch[dict.__repr__] = _pprint_dict

    def _format_dict_items(self, items, stream, indent, allowance, context,
                           level):
        write = stream.write
        indent += self._indent_per_level
        delimnl = ',\n' + ' ' * indent
        last_index = len(items) - 1

        for i, (key, ent) in enumerate(items):
            last = i == last_index
            rep = self._repr(key, context, level)
            write(rep + ': ')
            self._format(
                ent, stream, indent, (allowance if last else 1),
                context, level)
            if not last:
                write(delimnl)

    def _pprint_tpltemplate(self, obj, stream, indent, allowance, context, level):
        self._pprint_dict(obj.template, stream, indent, allowance, context, level)

    _dispatch[TplTemplate.__repr__] = _pprint_tpltemplate

    def _pprint_tpldictlist(self, obj, stream, indent, allowance, context, level):
        write = stream.write
        write('[')
        self._pprint_traversal(
            obj.traversal, stream, indent, allowance, context, level)
        write(' -> ')
        self._pprint_tpltemplate(
            obj.subtemplate, stream, indent, allowance, context, level)
        write(']')

    _dispatch[TplDictList.__repr__] = _pprint_tpldictlist

    def _pprint_traversal(self, obj, stream, indent, allowance, context, level):
        write = stream.write

        if obj is None:
            write('None')
            return

        write('(')

        ast = obj.ast
        write(' '.join(map(repr, ast.step_instructions)))

        write(')')

    _dispatch[Traversal.__repr__] = _pprint_traversal

    def _pprint_tpltraversal(self, obj, stream, indent, allowance, context, level):
        self._pprint_traversal(obj.traversal, stream, indent, allowance, context, level)

    _dispatch[TplTraversal.__repr__] = _pprint_tpltraversal

    def _pprint_tpllist(self, obj, stream, indent, allowance, context, level):
        write = stream.write

        write('[\n')
        write((indent + self._indent_per_level) * ' ')
        write('*')

        self._pprint_traversal(
            obj.traversal, stream, indent, allowance, context, level)

        write('\n')
        write(indent * ' ')
        write(']')

    _dispatch[TplList.__repr__] = _pprint_tpllist

    def _pprint_tplswitch(self, obj, stream, indent, allowance, context, level):
        write = stream.write
        write('<')
        self._pprint_traversal(
            obj.traversal, stream, indent, allowance, context, level)
        write('>?')
        self._pprint_dict(
            obj.cases, stream, indent, allowance, context, level)

    _dispatch[TplSwitch.__repr__] = _pprint_tplswitch

    def _pprint_tplcast(self, obj, stream, indent, allowance, context, level):
        write = stream.write

        write(obj.cast.__name__)
        write('(')

        self._format(
            obj.subtemplate, stream, indent, allowance, context, level)

        write(')')

    _dispatch[TplCast.__repr__] = _pprint_tplcast
