import logging

from eavlib.gremlin.traversal import __

from norm_api.util.batch import (
    TplCast, TplDictList, TplList, TplSwitch, TplTraversal)

LOG = logging.getLogger(__name__)


def InstitutionNameSnippet(inst_traversal):
    return inst_traversal.values("name")


def AttributionsSnippet(entities_traversal):
    return TplDictList(entities_traversal.out("attributions"), {
        "caption": __.values("caption"),
        "institution": InstitutionNameSnippet(__.out("institution")),
        "reference": ReferenceSnippet(__.out("reference")),
        "revision": InstitutionNameSnippet(__.out("revision")),
        })


def ReferenceSnippet(ref_traversal):
    return TplSwitch(ref_traversal.label(), {
        "TopographicReference": TplCast(dict, ref_traversal.value_map(
            "biblio", "excerpt", "topoform")),
        "URLReference": {"url": ref_traversal.values("url")},
        "VolumeReference": TplCast(dict, ref_traversal.value_map(
            "article", "line", "page", "volume")),
        })


def LabelsSnippet(traversal):
    return TplCast(
        lambda labels: {
            lbl["lang"]: lbl["text"] for lbl in labels
            } if labels is not None else {},
        TplList(
            traversal.out("labels").has("is_preferred", "true")
            .value_map("lang", "text"))  # attrs required, 0-to-n
        )


def IDandLabelsSnippet(traversal):
    return {
        "id": HHbIDSnippet(traversal),  # 1-1
        "labels": TplCast(
            lambda labels: {
                lbl["lang"]: lbl["text"] for lbl in labels
                } if labels is not None else {},
            TplList(
                traversal.out("labels").has("is_preferred", "true")
                .value_map("lang", "text"))  # attrs required, 0-to-n
            ),
        }


def ThesaurusConceptSnippet(traversal):
    return {
        "id": HHbIDSnippet(traversal),  # 1-1
        "terms": TplCast(
            lambda labels: {
                lbl["lang"]: lbl["text"] for lbl in labels
                } if labels is not None else {},
            TplList(
                traversal.out("terms").has("is_preferred", "true")
                .value_map("lang", "text"))  # attrs required, 0-to-n
            ),
        }


def HHbIDSnippet(entities_traversal):
    return TplCast(
        lambda hhb_id: int(hhb_id) if hhb_id is not None else None,
        entities_traversal.values("hhb_id"))  # required, 1-to-1


def LicenseSnippet(entity_traversal):
    return TplCast(  # 1-1
        dict,
        entity_traversal.out("license").value_map("name", "version", "url"))


def ValueMapList(traversal):
    # Hack to serialize NodeValueMap to dict
    def _dictlist(seq):
        return list(map(dict, seq))

    return TplCast(_dictlist, TplList(traversal, onempty=[]))


def DateSnippet(date_traversal):
    def wrapper(date):
        if not date:
            return None
        res = {}
        for k, v in date.items():
            if k in ("day", "month", "year", "size"):
                res[k] = int(v)
            else:
                res[k] = v
        return res

    return TplCast(wrapper, TplTraversal(date_traversal.value_map()))


def PersonNameUsageDictSnippet(pnus_traversal):
    def reformat_name_struct_(struct):
        res = dict(**struct)

        forename = " ".join(struct.get("forenames"))
        surname = " ".join(
            "%s %s" % (sn["namelink"], sn["surname"])
            if sn.get("namelink", None) else sn["surname"]
            for sn in struct.get("surnames", ()) if sn)
        ordinal = struct.get("ordinal", None)
        addnames = " ".join(struct.get("addnames", ()))

        res["fullname"] = " ".join(filter(None, (
            (forename, ordinal, surname, addnames))))

        return res

    return TplCast(reformat_name_struct_, {
        # "fullname": injected by reformat_name_struct_,
        "forenames": TplList(pnus_traversal.out("name").out("forenames").order().by("order", "ASC").out("forename").values("text")),
        "surnames": TplDictList(pnus_traversal.out("name").out("surnames").order().by("order", "ASC"), {
            "namelink": __.out("namelink").values("text"),
            "surname": __.out("surname").values("text"),
            }),
        "ordinal": pnus_traversal.out("ordinal").values("text"),
        "addnames": TplList(pnus_traversal.out("addnames").values("text")),
        })


def FamilyNameSnippet(fnus_traversal):
    return TplCast(
        lambda x: " ".join(
            "%s %s" % (sn["namelink"], sn["surname"])
            if sn.get("namelink", None) else sn["surname"]
            for sn in x if sn),
        TplDictList(fnus_traversal.out("names").order().by("order", "ASC"), {
            "namelink": __.out("namelink").values("text"),
            "surname": __.out("surname").values("text"),
            }))
