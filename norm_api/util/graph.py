from collections import OrderedDict

from eavlib.graph import Node
from eavlib.gremlin.traversal import TraverserCollection
from eavlib.gremlin.computer import Computer


from norm_api.util.misc import dict_multiget


def lookup_node_id(graph, label, hhb_id):
    g = graph.traversal()
    return next(g.v().has("hhb_id", hhb_id).has_label(label).id().limit(1), None)


# FIXME: This function should be called traversify or so
def nodeify(g, seq):
    yield from (g.v(el) for el in seq)


def eval_traversers(traversal):
    return Computer(traversal)._exec()


def traversers_map_by_origin(traversers, origins):
    res = OrderedDict.fromkeys(origins)

    for t in traversers:
        for i in range(len(t.path), 0, -1):
            if t.path[0:i] in res:
                res[t.path[0:i]] = t.value
                break
        else:
            # not found
            pass

    return res


def traversers_group_by_origin(traversers, origins):
    res = OrderedDict((o, []) for o in origins)

    for t in traversers:
        for i in range(len(t.path), 0, -1):
            if t.path[0:i] in res:
                res[t.path[0:i]].append(t.value)
                break
        else:
            # not found
            pass

    return res


def __magic_origins_detection(traversal):
    ast = traversal.ast

    if ast.step_instructions:
        if ast.step_instructions[0].op == "V":
            if ast.step_instructions[0].args:
                return tuple(map(Node, ast.step_instructions[0].args))
    for source_inst in ast.source_instructions:
        if source_inst.op == "withTraversers":
            collection = source_inst.args[0]
            if isinstance(collection, TraverserCollection):
                if collection.__type__ is Node:
                    return tuple(tuple(t.path) for t in collection)

    return None


def grouped(traversal, origins=None, listify=True):
    if origins is None:
        origins = __magic_origins_detection(traversal)
    if origins is None:
        raise ValueError("cannot determine origins")

    res = traversers_group_by_origin(eval_traversers(traversal), origins)

    return dict_multiget(res, origins) if listify else res


def ordered(traversal, origins=None, listify=True):
    if origins is None:
        origins = __magic_origins_detection(traversal)
    if origins is None:
        raise ValueError("cannot determine origins")

    mapped = traversers_map_by_origin(eval_traversers(traversal), origins)

    return dict_multiget(mapped, origins) if listify else mapped
