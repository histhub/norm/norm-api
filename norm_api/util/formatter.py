import re

from norm_api.util.misc import (maybeint, strtobool, trycast, trydict, trynext)

snake_case_re = re.compile(r"([a-z]|[0-9]|[_\.])*")


class FormatterHelpers:
    @classmethod
    def is_name_standard(cls, name_traversal):
        return strtobool(trynext(
            name_traversal.values("is_standard")))

    @classmethod
    def format_hhb_id_of(cls, traversal):
        return trycast(trynext(traversal.values("id")), maybeint)

    @classmethod
    def format_providers(cls, traversal):
        return traversal.out("provider").values("name")

    @classmethod
    def format_concept(cls, traversal):
        concept_id = trycast(trynext(traversal.id()), int)
        if not concept_id:
            return None

        labels = traversal.out("labels").has("is_preferred", "true").value_map(
            "lang", "text")

        return {
            "id": concept_id,
            "labels": {label["lang"]: label["text"] for label in labels}
            }

    @classmethod
    def format_existence(cls, existence_traversal):
        if next(existence_traversal.count()) <= 0:
            return None

        return {
            "providers": list(FormatterHelpers.format_providers(existence_traversal)),
            "start": FormatterHelpers.format_event(existence_traversal.out("start")),
            "end": FormatterHelpers.format_event(existence_traversal.out("end"))
            }

    @classmethod
    def format_periods(cls, dateinterval_traversal):
        if next(dateinterval_traversal.count()) <= 0:
            return None

        return {
            "start": FormatterHelpers.format_date(dateinterval_traversal.out("start")),
            "end": FormatterHelpers.format_date(dateinterval_traversal.out("end"))
            }

    @classmethod
    def format_event(cls, event_traversal):
        g = event_traversal.graph.traversal()
        try:
            t = g.v(next(event_traversal))
        except StopIteration:
            return None

        try:
            next(event_traversal)
        except StopIteration:
            pass
        else:
            raise ValueError("traversal results in multiple results")

        return {
            "type": FormatterHelpers.format_concept(t.out("type")),
            **FormatterHelpers.format_date(t.out("when"))
            }

    @classmethod
    def format_date(cls, date_traversal):
        return {
            "date": trynext(date_traversal.out("labels").values("text")),
            "precision": trydict(trynext(date_traversal.value_map(
                "type", "size", "unit", "mode")))
            }
