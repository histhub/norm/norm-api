import bottle
import re
import urllib.parse


def strtobool(s):
    return s.lower() in ("true", "yes", "1", "on")


def trynext(iter, fallback=None):
    try:
        return next(iter)
    except StopIteration:
        return fallback


def trydict(iter, fallback=None):
    try:
        return dict(iter)
    except TypeError:
        return fallback


def trycast(x, type_func):
    return type_func(x) if x is not None else None


def dict_nestset(dct, keys, value, autodict=True):
    if isinstance(keys, (tuple, list)):
        p = dct
        for k in keys[:-1]:
            try:
                p = p[k]
            except KeyError:
                if autodict:
                    p[k] = {}
                    p = p[k]
                else:
                    raise
        p[keys[-1]] = value
    else:
        dct[keys] = value


def dict_multiget(dct, keys, fallback=None):
    return tuple(map(lambda k: dct.get(k, fallback), keys))


def truthy(v):
    if isinstance(v, (tuple, list)):
        return any(v)
    elif isinstance(v, dict):
        return any(v.values())
    return bool(v)


def dictlist_fill(seq, keys, data, prefill=lambda x: x, omit_empty=True,
                  autodict=None):
    if autodict is None:
        autodict = omit_empty

    for dct, v in zip(seq, map(prefill, data)):
        if not omit_empty or truthy(v):
            dict_nestset(dct, keys, v, autodict=autodict)


def maybeint(s):
    return int(s) if isinstance(s, str) and s.isdigit() else s


def maybedict(x):
    return dict(x) if x is not None else None


def url_format(base_url=None, path=None, query=dict()):
    if base_url is None:
        base_url = "%s://%s%s" % bottle.request.urlparts[:3]
    url = base_url
    if path is not None:
        url = urllib.parse.urljoin(url + "/", path)
    if query:
        return "%s?%s" % (url, "&".join(
            "%s=%s" % i for i in query.items()))
    return url


def select_label(labels, lang="deu", default=""):
    for lang in (lang, "eng", "deu", "fra", "ita", "roh", "lat"):
        if lang in labels:
            return labels[lang]
    return next(iter(labels.values())) if labels else default


def format_volume_reference(reference):
    html = ""
    if reference.get("volume", None):
        html += reference["volume"]
        if reference.get("article", None):
            html += ", Art. " + reference["article"]
        if reference.get("page", None):
            html += ", (S. " + reference["page"]
            if reference.get("line", None):
                html += ", Zeile " + reference["line"]
            html += ")"
    return html


def render_commonmark(src):
    try:
        import commonmark
        html = commonmark.commonmark(src)
        html = html.rstrip()

        # Get rid of wrapping paragraph
        if html.startswith("<p>"):
            html = html[3:]
        if html.endswith("</p>"):
            html = html[:-4]

        return html
    except ImportError:
        return src


class ExtURLMaker:
    @classmethod
    def make(cls, institution, external_id):
        func = getattr(
            cls, "make_%s" % re.sub(r"\W", "_", institution.lower()), None)

        if not callable(func):
            return None

        return func(external_id)

    @classmethod
    def make_gnd(cls, gnd_id):
        # NOTE: GND numbers are %s not %u because the last character can be X.
        return "http://d-nb.info/gnd/%s" % (gnd_id)

    @classmethod
    def make_hls(cls, hls_id):
        return "http://hls-dhs-dss.ch/de/articles/%06u/" % (hls_id)

    @classmethod
    def make_ortsnamen_ch(cls, onch_id):
        return "https://search.ortsnamen.ch/record/%u" % (onch_id)

    @classmethod
    def make_ssrq(cls, ssrq_id):
        prefix = ssrq_id[:3]

        if prefix in ("per", "org"):
            return "http://www.ssrq-sds-fds.ch/persons-db-edit/?query=%s" % (ssrq_id)
        elif prefix == "loc":
            return "http://www.ssrq-sds-fds.ch/places-db-edit/views/view-place.xq?id=%s" % (ssrq_id)
        elif prefix == "key":
            return "https://www.ssrq-sds-fds.ch/lemma-db-edit/views/view-keyword.xq?id=%s" % (ssrq_id)
        elif prefix == "lem":
            return "http://www.ssrq-sds-fds.ch/lemma-db-edit/views/view-lemma.xq?id=%s" % (ssrq_id)
        else:
            return None


class HHbDateFormatter:
    @staticmethod
    def mode2sgn(mode):
        if mode == "after":
            return "+"
        elif mode == "before":
            return "-"
        else:
            return "?"

    @classmethod
    def format(cls, date):
        res = date.get("type", "?") + " "

        if all(x in date for x in ("day", "month", "year")):
            res += "%02u. %02u. %04u" % (date["day"], date["month"], date["year"])
        elif all(x in date for x in ("month", "year")):
            res += "%02u. %04u" % (date["month"], date["year"])
        elif "year" in date:
            res += "%04u" % date["year"]

        if date.get("calendar", None):
            res += " (%s)" % date["calendar"]

        if all(x in date for x in ()):
            res += " (~%c %u %s)" % (
                cls.mode2sgn(date["mode"]), date["size"], date["unit"])

        return res


class GeometryFormatter:
    @classmethod
    def extract_points(cls, s):
        return re.findall(r"POINT\s*\(([^)]*)\)", s)

    @classmethod
    def format(cls, srid, s):
        if srid == 21781:
            pts = cls.extract_points(s)
            if len(pts) > 1:
                # Polygon
                return "Polygon <...> (CH1903 / LV03)"
            else:
                # Point
                return "%u %u (CH1903 / LV03)" % tuple(map(int, pts[0].split(" ")))


class AttributionsManager:
    def __init__(self):
        self.attributions = []

    def add(self, attribution):
        i = 0
        for i, a in enumerate(self.attributions, 1):
            if a == attribution:
                return i

        self.attributions.append(attribution)
        return i + 1

    def footnotes(self):
        return enumerate(self.attributions, 1)
