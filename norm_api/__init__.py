# Metadata

__description__ = "The official API to query the histHub Norm database"

__author__ = "Rechtsquellenstiftung des Schweizerischen Juristenvereins"
__copyright__ = ("Copyright 2020 %s" % __author__)
__license__ = "GPLv3"
__maintainer__ = __author__
# Versions as per PEP 440 (https://www.python.org/dev/peps/pep-0440/)
__version_info__ = (0, 2, 0)
__version_suffix__ = "dev1"
__version__ = (".".join(map(str, __version_info__))
               + (__version_suffix__ if __version_suffix__ else ""))
